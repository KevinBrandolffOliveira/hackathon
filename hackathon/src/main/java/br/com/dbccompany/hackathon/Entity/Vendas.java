package br.com.dbccompany.hackathon.Entity;

import java.util.List;

public class Vendas {
    private final Integer ID = 003;
    private Integer idVenda;
    private List<Item> itens;
    private String vendedor;

    public Vendas(Integer idVenda, List<Item> itens, String vendedor) {
        this.idVenda = idVenda;
        this.itens = itens;
        this.vendedor = vendedor;
    }

    public Integer getID() {
        return ID;
    }

    public Integer getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(Integer idVenda) {
        this.idVenda = idVenda;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }
}
