package br.com.dbccompany.hackathon.writer;

public class Employee {
    String id;
    String firstName;
    String lastName;
    String thirdInfo;

    public Employee(String id, String firstName, String lastName, String thirdInfo) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.thirdInfo = thirdInfo;
    }

    public Employee() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getThirdInfo() {
        return thirdInfo;
    }

    public void setThirdInfo(String thirdInfo) {
        this.thirdInfo = thirdInfo;
    }
}
