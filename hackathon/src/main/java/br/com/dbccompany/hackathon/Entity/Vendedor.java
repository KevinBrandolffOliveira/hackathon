package br.com.dbccompany.hackathon.Entity;

public class Vendedor {
    private final Integer ID = 001;
    private Character[] cpf = new Character[11];
    private String nome;
    private Double salario;

    public Vendedor(Character[] cpf, String nome, Double salario) {
        this.cpf = cpf;
        this.nome = nome;
        this.salario = salario;
    }

    public Integer getID() {
        return ID;
    }

    public Character[] getCpf() {
        return cpf;
    }

    public void setCpf(Character[] cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }
}
