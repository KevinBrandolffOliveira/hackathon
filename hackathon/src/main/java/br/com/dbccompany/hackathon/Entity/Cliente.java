package br.com.dbccompany.hackathon.Entity;

public class Cliente {
    private final Integer ID = 002;
    private Character[] cnpj = new Character[14];
    private String nome;
    private String area;

    public Cliente(Character[] cnpj, String nome, String area) {
        this.cnpj = cnpj;
        this.nome = nome;
        this.area = area;
    }

    public Integer getID() {
        return ID;
    }

    public Character[] getCnpj() {
        return cnpj;
    }

    public void setCnpj(Character[] cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
