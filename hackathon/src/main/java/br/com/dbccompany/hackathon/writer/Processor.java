package br.com.dbccompany.hackathon.writer;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component(value = "processador")
public class Processor implements ItemProcessor<Employee, Employee> {

    private Integer quantidadeCliente;

    @Override
    public Employee process(Employee item) throws Exception {
            if ( item.id == "002" ){
                this.quantidadeCliente += 1;
            }
            return quantidadeCliente;
    }
}
