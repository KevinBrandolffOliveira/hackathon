package br.com.dbccompany.hackathon.writer;

public class Dados {

    private String id;
    private String firstInfo;
    private String secondInfo;
    private String thirdInfo;

    public Dados(String id, String firstInfo, String secondInfo, String thirdInfo) {
        this.id = id;
        this.firstInfo = firstInfo;
        this.secondInfo = secondInfo;
        this.thirdInfo = thirdInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstInfo() {
        return firstInfo;
    }

    public void setFirstInfo(String firstInfo) {
        this.firstInfo = firstInfo;
    }

    public String getSecondInfo() {
        return secondInfo;
    }

    public void setSecondInfo(String secondInfo) {
        this.secondInfo = secondInfo;
    }

    public String getThirdInfo() {
        return thirdInfo;
    }

    public void setThirdInfo(String thirdInfo) {
        this.thirdInfo = thirdInfo;
    }
}
